
let vidsElem = $("#vids");
let apiUrl = 'http://carlin.uit.yorku.ca/faculty/relay/relayrss/2019/139959.xml';
let proxy = 'https://cors-anywhere.herokuapp.com/';

let finalURL = proxy + apiUrl;

function genVidElems(title, videoLink, pubDate) {
    let parentElem = document.createElement('div');
    let bodyElem = document.createElement('div');
    let titleElem = document.createElement('h5');
    let videoParentElem = document.createElement('div');
    let videoElem = document.createElement('video');
    let videoSourceElem = document.createElement('source');
    let pubDateElem = document.createElement('p');
    let linkElem = document.createElement('a');

    // Video Link Element
    linkElem.setAttribute('href', videoLink);
    linkElem.setAttribute('class', 'btn btn-primary');
    linkElem.appendChild(document.createTextNode('Video link'));
    
    // Published Data Element
    pubDateElem.appendChild(document.createTextNode(pubDate));

    // Video Title Element
    titleElem.setAttribute('class', 'card-title');
    titleElem.appendChild(document.createTextNode(title));

    // Body Element
    // Includes: Title, Date Publish, and Video Link
    bodyElem.setAttribute('class', 'card-body');
    bodyElem.setAttribute('class', 'card-body');
    bodyElem.appendChild(titleElem);
    bodyElem.appendChild(pubDateElem);
    bodyElem.appendChild(linkElem);

    // video Element
    videoElem.setAttribute('class', 'card-img-top embed-responsive-item');
    videoElem.setAttribute('controls', '');

    // video source Element
    videoSourceElem.setAttribute('src', videoLink);
    videoSourceElem.setAttribute('type', "video/mp4");
    
    // video Element adding child video source
    videoElem.appendChild(videoSourceElem);

    // video parent frame Element adding child video
    videoParentElem.setAttribute('class', 'embed-responsive embed-responsive-16by9');
    videoParentElem.appendChild(videoElem);

    // parent card element
    parentElem.setAttribute('class', 'card col-md-6');
    
    // parent card has cildren video and body elems
    parentElem.appendChild(videoElem);
    parentElem.appendChild(bodyElem);
    vidsElem.append(parentElem);
}

$(document).ready(function () {
    $.get(finalURL, { dataType: "xml", cache: false }, function(xml) {
        // console.log("xml", xml);
        $(xml).find('item').each(function() {
            let title = $(this).find('title').text();
            let author = $(this).find('itunes:author').text();
            // let subtitle = $(this).find('itunes:subtitle').text();
            // let summar = $(this).find('itunes:summar').text();
            // let description = $(this).find('description').text();
            // let enclosure = $(this).find('enclosure').text();
            let guid = $(this).find('guid').text();
            let pubDate = $(this).find('pubDate').text();
            // let duration = $(this).find('itunes:duration').text();
            // let keywords = $(this).find('itunes:keywords').text();
            // console.log(author);
            genVidElems(title, guid, pubDate);
        });
    });
});
